function CustomResponceHandler(res, data = { statusCode: 400 }) {
    switch (data.statusCode) {
        case 200:
            res.status(data.statusCode).send(data);
            break;
        case 201:
            res.status(data.statusCode).send(data);
            break;
        case 204://when only send success message 
            res.status(data.statusCode).send({ statusCode: data.statusCode, message: data.message });
            break;
        case 404:
            res.status(data.statusCode).send({ statusCode: data.statusCode, message: data.result });
            break;
        case 422:
            // console.log(data.statusCode)
            res.status(data.statusCode).send({ statusCode: data.statusCode, message: data.result.error.details[0].message });
            // res.status(data.statusCode)
            break;
        case 409:
            res.status(data.statusCode).send({ statusCode: data.statusCode, message: data.result });
            break;
        case 406:
            res.status(data.statusCode).send({ statusCode: data.statusCode, message: data.result });
            break;
        case 403:
            res.status(data.statusCode).send({ statusCode: data.statusCode, message: data.result });
            break;
        default:
            res.status(400).send({ message: "Something Went Wrong", err: data.err });
    }
}

module.exports = { CustomResponceHandler };