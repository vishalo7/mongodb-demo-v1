const express = require('express');
const appRoute = express();
const { UserList, UserInsert, UserUpdate, GetSingleUser, UserDelete } = require('./User/index');

appRoute.get('/list', (req, res) => {
    UserList(req, res)
});

appRoute.get('/edit/:id', (req, res) => {
    GetSingleUser(req, res)
});

appRoute.post('/add', (req, res) => {
    UserInsert(req, res)
});

appRoute.post('/update', (req, res) => {
    UserUpdate(req, res)
});

appRoute.post('/delete', (req, res) => {
    UserDelete(req, res)
});


module.exports = appRoute;