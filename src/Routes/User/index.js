const Joi = require('joi');
// const ObjectId = require('mongoose').Types.ObjectId;

const { Select, Insert, SelectSingle, Update, Delete } = require('../../Query/index');
const { Users } = require('../../model/index');
const { UsersSchema } = require('../../schema/index');
const { CustomResponceHandler } = require('../../../urtils/index');

const UserList = (req, res) => {
    Select(Users).then((user) => {
        return CustomResponceHandler(res, { statusCode: 200, result: user });
    }).catch((err) => {
        return CustomResponceHandler(res, err.statusCode ? err : { statusCode: 400 });
    });
}

const UserInsert = (req, res) => {

    let validates = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        phone: req.body.phone,
        address: req.body.address,
        email: req.body.email,
        password: req.body.password,
    };

    const result = Joi.validate(validates, UsersSchema.UserAddSchema);

    if (result.error) return CustomResponceHandler(res, { statusCode: 422, result });

    Insert(Users, validates).then((user) => {
        return CustomResponceHandler(res, { statusCode: 200, result: user })
    }).catch((err) => {
        return CustomResponceHandler(res, err.statusCode ? err : { statusCode: 400 })
    });
}

const UserUpdate = (req, res) => {
    const id = { _id: req.body.id };

    const resultId = Joi.validate(id, UsersSchema.EditId);

    if (resultId.error) return CustomResponceHandler(res, { statusCode: 422, result: resultId });

    let validates = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        phone: req.body.phone,
        address: req.body.address,
        email: req.body.email,
        password: req.body.password,
    };
    const result = Joi.validate(validates, UsersSchema.UserAddSchema);

    if (result.error) return CustomResponceHandler(res, { statusCode: 422, result });

    Update(Users, id, validates).then((user) => {
        return CustomResponceHandler(res, { statusCode: 200, result: user });
    }).catch((err) => {
        return CustomResponceHandler(res, err.statusCode ? err : { statusCode: 400 });
    });

}

const UserDelete = (req, res) => {
    const id = { _id: req.body.id };
    const result = Joi.validate(id, UsersSchema.UsersDeleteSchema);

    if (result.error) return CustomResponceHandler(res, { statusCode: 422, result });

    Delete(Users, id).then((user) => {
        return CustomResponceHandler(res, { statusCode: 200, result: user });
    }).catch((err) => {
        return CustomResponceHandler(res, err.statusCode ? err : { statusCode: 400 });
    });
}

const GetSingleUser = (req, res) => {
    const id = { _id: req.params.id };

    const result = Joi.validate(id, UsersSchema.EditId);

    if (result.error) return CustomResponceHandler(res, { statusCode: 422, result });

    SelectSingle(Users, id).then((user) => {
        return CustomResponceHandler(res, { statusCode: 200, result: user })
    }).catch((err) => {
        return CustomResponceHandler(res, err.statusCode ? err : { statusCode: 400 })
    });
}


module.exports = { UserList, UserInsert, UserUpdate, GetSingleUser, UserDelete };