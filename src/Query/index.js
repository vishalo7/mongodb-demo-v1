// select Query
const Select = (modelObj = {}) => modelObj.find();

// insert Data
const Insert = (modelObj = {}, InsertObj = {}) => {
    let iObj = modelObj(InsertObj);
    return iObj.save();
}

// get single record
const SelectSingle = (modelObj, editId) => modelObj.findById(editId);

// update record
const Update = (modelObj = {}, updateId = '', UpdateObj = {}) => modelObj.findByIdAndUpdate(updateId, { $set: UpdateObj }, { new: true });

// delete single record
const Delete = (modelObj = {}, deleteId = '') => (modelObj.findByIdAndRemove(deleteId, { new: false }))

module.exports = {Select, Insert, SelectSingle, Update, Delete};