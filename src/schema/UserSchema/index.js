const Joi = require('joi');
let UsersSchema = {
    UserAddSchema: Joi.object().keys({
        firstName: Joi.string().required().error(() => 'First Name is required'),
        lastName: Joi.string().required().error(() => 'Last Name is required'),
        phone: Joi.string().required().error(() => 'Phone is required'),
        address: Joi.string().required().error(() => 'Address is required'),
        email: Joi.string().required().error(() => 'email is required'),
        password: Joi.string().required().error(() => 'password is required'),
    }),

    UsersDeleteSchema: Joi.object().keys({ _id: Joi.string().required().error(() => 'id  is required') }),

    EditId: Joi.object().keys({ _id: Joi.string().required().error(() => 'id is required') }),
    
}
module.exports = UsersSchema 
