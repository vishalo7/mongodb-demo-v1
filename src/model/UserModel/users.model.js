const mongoose = require('mongoose');

var Users = mongoose.model('users', {
    firstName: { type: String },
    lastName:{type:String},
    phone: { type: String },
    address: { type: String },
    email: { type: String,required: true },
    password:{ type: String,required: true },
});

module.exports = { Users };

